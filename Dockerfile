FROM openjdk:17
RUN mkdir /home/project
WORKDIR /home/project
COPY ./target/test-demo-task-0.0.1.jar .
CMD ["java", "-jar", "test-demo-task-0.0.1.jar"]
EXPOSE 8080