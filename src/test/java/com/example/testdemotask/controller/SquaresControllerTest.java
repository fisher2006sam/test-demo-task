package com.example.testdemotask.controller;

import com.example.testdemotask.handler.AnswerException;
import com.example.testdemotask.records.ResponseData;
import com.example.testdemotask.service.SquareService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SquaresController.class)
class SquaresControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SquareService service;

    @Test
    void checkSquaresDifferent() throws Exception {
        String requestBody = """
                {
                    "a": 2,
                    "b" : -1,
                    "c": -15
                }
                """;
        String requestResponse = "{\"x1\":3.0,\"x2\":-2.5}";

        Mockito.when(service.calculateSquares(anyInt(), anyInt(), anyInt())).thenReturn(new ResponseData(3.0, -2.5));
        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(requestResponse));
    }

    @Test
    void checkBadSquares() throws Exception {
        String requestBody = """
                {
                    "a": 4,
                    "b": -1,
                    "c": -15
                }
                """;

        Mockito.when(service.calculateSquares(anyInt(), anyInt(), anyInt())).thenThrow(new AnswerException());
        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is4xxClientError());
    }

    @Test
    void checkSquaresSame() throws Exception {
        String requestBody = """
                {
                    "a": 1,
                    "b" : -6,
                    "c": 9
                }
                """;
        String requestResponse = "{\"x1\":3.0,\"x2\":3.0}";

        Mockito.when(service.calculateSquares(anyInt(), anyInt(), anyInt())).thenReturn(new ResponseData(3.0, 3.0));
        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(requestResponse));
    }

    @Test
    void checkBadSquaresInRequest() throws Exception {
        String requestBody = """
                {
                    "a": "s",
                    "b" : -1,
                    "c": -15
                }
                """;

        Mockito.when(service.calculateSquares(anyInt(), anyInt(), anyInt())).thenThrow(new AnswerException());
        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is4xxClientError());
    }
}