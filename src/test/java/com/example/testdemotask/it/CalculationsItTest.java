package com.example.testdemotask.it;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class CalculationsItTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void checkCalculationsSuccess() throws Exception {
        String requestBody = """
                {
                    "a": 2,
                    "b" : -1,
                    "c": -15
                }
                """;
        String requestResponse = "{\"x1\":3.0,\"x2\":-2.5}";

        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(requestResponse));
    }

    @Test
    void checkCalculationsSuccessSameRes() throws Exception {
        String requestBody = """
                {
                    "a": 1,
                    "b" : -6,
                    "c": 9
                }
                """;
        String requestResponse = "{\"x1\":3.0,\"x2\":3.0}";

        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(requestResponse));
    }

    @Test
    void checkBadValuesInRequest() throws Exception {
        String requestBody = """
                {
                    "a": "s",
                    "b" : -1,
                    "c": -15
                }
                """;

        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is4xxClientError());
    }

    @Test
    void checkBadDiscriminant() throws Exception {
        String requestBody = """
                {
                    "a": -4,
                    "b": -1,
                    "c": -15
                }
                """;

        mockMvc.perform(MockMvcRequestBuilders.post("/square")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is4xxClientError());
    }
}
