package com.example.testdemotask.service;

import com.example.testdemotask.dto.CalculationData;
import com.example.testdemotask.handler.AnswerException;
import com.example.testdemotask.records.ResponseData;
import com.example.testdemotask.repository.CalculationsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class SquareServiceTest {

    @Autowired
    private SquareService service;

    @MockBean
    private CalculationsRepository repository;

    @Test
    void calculateSquaresSuccess() throws AnswerException {
        when(repository.save(any())).thenReturn(new CalculationData());

        ResponseData responseData = service.calculateSquares(2, -1, -15);
        Assertions.assertEquals(responseData.x1(), 3.0);
        Assertions.assertEquals(responseData.x2(), -2.5);
    }

    @Test
    void calculateSquaresBad() {
        when(repository.save(any())).thenReturn(new CalculationData());
        Assertions.assertThrows(AnswerException.class, () -> service.calculateSquares(2, -1, 15));
    }
}