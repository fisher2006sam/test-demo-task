package com.example.testdemotask.repository;

import com.example.testdemotask.dto.CalculationData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CalculationsRepositoryTest {

    @Autowired
    private CalculationsRepository repository;

    @Test
    @Transactional
    void checkAddingDataToDB() {
        CalculationData calculationData = CalculationData.builder()
                .a(1)
                .b(2)
                .c(3)
                .x1(4.0)
                .x2(5.0)
                .date(LocalDateTime.now())
                .build();
        repository.save(calculationData);
        assertThat(calculationData.getId()).isGreaterThan(0);
    }

    @Test
    @Transactional
    void checkAddAndFindDataToDB() {
        CalculationData calculationData = CalculationData.builder()
                .a(1)
                .b(2)
                .c(3)
                .x1(4.0)
                .x2(5.0)
                .date(LocalDateTime.now())
                .build();
        repository.save(calculationData);

        CalculationData data = repository.findById(calculationData.getId()).orElse(new CalculationData());

        assertNotNull(data);
        assertEquals(data.getA(), 1);
        assertEquals(data.getB(), 2);
        assertEquals(data.getC(), 3);
        assertEquals(data.getX1(), 4.0);
        assertEquals(data.getX2(), 5.0);
    }
}