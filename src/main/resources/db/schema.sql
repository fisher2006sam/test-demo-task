-- DROP SCHEMA IF EXISTS square_data CASCADE;

CREATE SCHEMA IF NOT EXISTS square_data;

CREATE TABLE IF NOT EXISTS square_data.calculations(
    id SERIAL NOT NULL PRIMARY KEY,
    a int,
    b int,
    c int,
    x1 double precision,
    x2 double precision,
    datetime TIMESTAMP
)