package com.example.testdemotask.records;

public record ResponseData(Double x1, Double x2) {
}
