package com.example.testdemotask.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "calculations", schema = "square_data")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CalculationData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Integer a;
    private Integer b;
    private Integer c;

    private Double x1;
    private Double x2;

    @Column(name = "datetime")
    private LocalDateTime date;
}
