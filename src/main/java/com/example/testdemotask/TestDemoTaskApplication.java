package com.example.testdemotask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDemoTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestDemoTaskApplication.class, args);
    }

}
