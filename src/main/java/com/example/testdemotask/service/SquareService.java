package com.example.testdemotask.service;

import com.example.testdemotask.descriptions.Descriptions;
import com.example.testdemotask.dto.CalculationData;
import com.example.testdemotask.handler.AnswerException;
import com.example.testdemotask.records.ResponseData;
import com.example.testdemotask.repository.CalculationsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
public class SquareService {

    private final CalculationsRepository calculationsRepository;

    public SquareService(CalculationsRepository calculationsRepository) {
        this.calculationsRepository = calculationsRepository;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public ResponseData calculateSquares(Integer a, Integer b, Integer c) throws AnswerException {

        int d = (b * b) - (4 * a * c);
        log.info("Получен дискриминант: " + d);

        if (d < 0) {
            throw new AnswerException(LocalDateTime.now(), Descriptions.DESCR_BLOW_ZERO);
        }

        double discriminantSqrt = Math.sqrt(d);
        log.info(String.format("%s : %.2f", "Корень дескриминанта (по модулю): ", discriminantSqrt));

        double x1;
        double x2;
        if (d == 0) {
            x1 = x2 = (-b + discriminantSqrt)/(2 * a);
            log.info(String.format("%s : %.2f, %.2f", "Так как дискриминант равен 0, то X1 равен X2", x1, x2));
        } else {
            x1 = (-b + discriminantSqrt)/(2 * a);
            x2 = (-b - discriminantSqrt)/(2 * a);
            log.info(String.format("%s : %.2f, %.2f", "Полученные X1 и X2", x1, x2));
        }

        CalculationData calculationData = CalculationData.builder()
                .a(a)
                .b(b)
                .c(c)
                .x1(x1)
                .x2(x2)
                .date(LocalDateTime.now())
                .build();
        calculationsRepository.save(calculationData);

        return new ResponseData(x1, x2);
    }
}
