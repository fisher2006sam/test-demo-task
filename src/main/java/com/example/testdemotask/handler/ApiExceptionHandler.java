package com.example.testdemotask.handler;

import com.example.testdemotask.descriptions.Descriptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AnswerException.class)
    public ResponseEntity<AnswerException> answerError(Exception exception, WebRequest request) {
        AnswerException responseBody = AnswerException.builder()
                .timeStamp(LocalDateTime.now())
                .message(String.format("%s : %s", Descriptions.ANSWER_ERROR, exception.getMessage()))
                .build();
        return ResponseEntity.badRequest().body(responseBody);
    }

}
