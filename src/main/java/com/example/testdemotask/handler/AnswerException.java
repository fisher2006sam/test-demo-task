package com.example.testdemotask.handler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.function.Supplier;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnswerException extends Exception implements Supplier<AnswerException> {

    private LocalDateTime timeStamp;
    private String message;

    @Override
    public AnswerException get() {
        return this;
    }
}
