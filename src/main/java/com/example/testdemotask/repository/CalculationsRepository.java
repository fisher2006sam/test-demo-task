package com.example.testdemotask.repository;

import com.example.testdemotask.dto.CalculationData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculationsRepository extends CrudRepository<CalculationData, Long> {
}
