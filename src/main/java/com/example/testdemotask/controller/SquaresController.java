package com.example.testdemotask.controller;

import com.example.testdemotask.records.RequestData;
import com.example.testdemotask.records.ResponseData;
import com.example.testdemotask.service.SquareService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class SquaresController {

    private final SquareService service;

    public SquaresController(SquareService service) {
        this.service = service;
    }

    @PostMapping("/square")
    public ResponseEntity<ResponseData> getSquares(@RequestBody RequestData data) throws Exception {
        log.info("Поступил запрос на контроллер для нахождения квадратного корня уравнения");
        log.info(String.format("%s : %d, %d, %d", "Вводные параметры", data.a(), data.b(), data.c()));
        return ResponseEntity.ok(service.calculateSquares(data.a(), data.b(), data.c()));
    }
}
